import 'package:challenge_flutter_colab_app/models/user.model.dart';
import 'package:challenge_flutter_colab_app/repositores/user.repository.dart';
import 'package:mobx/mobx.dart';

part 'user.controller.g.dart';

class UserController = _UserController with _$UserController;

abstract class _UserController with Store {
  UserRepository _repository;

  _UserController() {
    _repository = UserRepository();
  }

  @observable
  UserModel userModel;

  Future<UserModel> loadUserItems() async {
    try {
      userModel = await _repository.getItems();
    } catch (e) {
      throw Exception(e);
    }
    return userModel;
  }
}
