import 'package:challenge_flutter_colab_app/views/user.views.dart';
import 'package:flutter/material.dart';

class Routes {
  Routes._();

  static const INITIAL = "/";

  static final routes = <String, WidgetBuilder>{
    INITIAL: (BuildContext context) => UserPage(),
  };
}
