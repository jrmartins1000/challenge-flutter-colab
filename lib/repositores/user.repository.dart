import 'dart:convert';

import 'package:challenge_flutter_colab_app/utilities/constants.dart';
import 'package:http/http.dart' as http;
import 'package:challenge_flutter_colab_app/interfaces/user.interface.dart';
import 'package:challenge_flutter_colab_app/models/user.model.dart';

class UserRepository implements IUserRepository {
  @override
  Future<UserModel> getItems() async {
    UserModel model;

    try {
      final response =
          await http.get('${Constants.BASE_URL}${Constants.USER_LIST}');

      if (response.statusCode == 200) {
        model = UserModel.fromJson(jsonDecode(response.body));
      } else {
        print("Erro ao carregar lista" + response.statusCode.toString());
        return null;
      }
    } catch (error, stacktrace) {
      print("Erro ao carregar lista" + stacktrace.toString());
      return null;
    }
    return model;
  }
}
