import 'package:challenge_flutter_colab_app/models/user.model.dart';

abstract class IUserRepository {
  Future<UserModel> getItems();
}
