
import 'package:flutter/material.dart';


class CustomCardItem extends StatelessWidget {
  final model;
  final Widget iconWidget;

  CustomCardItem(this.model, this.iconWidget);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(5),
        margin: EdgeInsets.symmetric(vertical: 6, horizontal: 18, ),
        child: Row(
          children: <Widget>[
            iconWidget,
            Flexible(
                flex: 1,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      model.name,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17.0,
                          color: Colors.deepPurple,
                      ),
                    ),
                    Text(
                      model.description,
                      style: TextStyle(
                          fontSize: 13.0,
                          color: Colors.black54
                      ),
                    )
                  ],
                )
            )
          ],
        )
    );
  }
}