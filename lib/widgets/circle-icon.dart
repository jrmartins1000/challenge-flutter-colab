import 'package:challenge_flutter_colab_app/utilities/size_config.utilities.dart';
import 'package:flutter/material.dart';

class CircleIcon extends StatelessWidget {
  final String icon;

  CircleIcon(this.icon);

  @override
  Widget build(BuildContext context) {
    final sizeConfig = SizeConfig(mediaQueryData: MediaQuery.of(context));
    return Container(
      margin: EdgeInsets.only(right: 10),
      child: Column(
        children: <Widget>[
          Container(
            width: sizeConfig.dynamicScaleSize(size: 60, scaleFactorTablet: 2),
            height: sizeConfig.dynamicScaleSize(size: 60, scaleFactorTablet: 2),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
            ),
            child: CircleAvatar(
              radius:
                  sizeConfig.dynamicScaleSize(size: 73, scaleFactorTablet: 2),
              backgroundImage: NetworkImage(icon),
            ),
          ),
        ],
      ),
    );
  }
}
