import 'package:challenge_flutter_colab_app/controllers/user.controller.dart';
import 'package:challenge_flutter_colab_app/models/user.model.dart';
import 'package:challenge_flutter_colab_app/widgets/circle-icon.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserPage extends StatefulWidget {
  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  Future _future;
  UserController controller;

  void initState() {
    controller = UserController();
    _future = _loadData();

    super.initState();
  }

  Future<UserModel> _loadData() async {
    return await controller.loadUserItems();
  }

  @override
  Widget build(BuildContext context) {
    return _bodyItens();
  }

  Widget _bodyItens() {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: Container(
                  width: 220,
                  margin: EdgeInsets.only(top: 50),
                  child: Image.asset('assets/images/colab.png'),
                ),
              ),
            ],
          ),
          Container(
            child: FutureBuilder<UserModel>(
              future: _future,
              builder: (context, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                    break;
                  case ConnectionState.waiting:
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CircularProgressIndicator(),
                        ],
                      ),
                    );
                    break;
                  case ConnectionState.active:
                    break;
                  case ConnectionState.done:
                    if (snapshot.hasData) {
                      return Expanded(
                        child: _createListBuilder(snapshot.data),
                      );
                    } else {
                      return Text('Nenhum dado de usuario encontrado');
                    }
                    break;
                }
                return Text('Error load');
              },
            ),
          ),
        ],
      ),
    );
  }

  ListView _createListBuilder(UserModel model) {
    return ListView.builder(
        physics: const AlwaysScrollableScrollPhysics(),
        itemCount: model.results.length,
        itemBuilder: (context, index) {
          return _mountMenuItem(context, model.results[index]);
        });
  }

  Widget _mountMenuItem(context, Result model) {
    return InkWell(
      child: Container(
        padding: EdgeInsets.all(5),
        margin: EdgeInsets.symmetric(
          vertical: 6,
          horizontal: 18,
        ),
        child: Row(
          children: <Widget>[
            CircleIcon(
              model.picture.medium.toString(),
            ),
            SizedBox(
              width: 10,
            ),
            Flexible(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    model.name.first,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                      //color: Constants.MAIN_COLOR,
                    ),
                  ),
                  Text(
                    model.name.last,
                    style: TextStyle(fontSize: 14.0, color: Colors.black54),
                  ),
                  Text(
                    model.email,
                    style: TextStyle(fontSize: 13.0, color: Colors.black54),
                  ),
                ],
              ),
            ),
          ],
        ),

        // child: Text(model.name.title),
      ),
    );
  }
}
